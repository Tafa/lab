import Vue from 'vue'
import App from './App.vue'
import VueRouter from 'vue-router'
import SortingVisualisationComponent from './components/SortingVisualisationComponent.vue'
import DashboardComponent from './components/DashboardComponent.vue'
import SnakeGame from './components/SnakeGameComponent.vue'

Vue.use(VueRouter)

Vue.config.productionTip = false

const router = new VueRouter({
  base: __dirname,
  routes: [
    {
      path: '/',
      name: 'dashboard',
      component: DashboardComponent
    },
    {
      path: '/sorting-visualisation',
      name: 'sortingVisualisation',
      component: SortingVisualisationComponent
    },
    {
      path: '/snake-game',
      name: 'snakeGame',
      component: SnakeGame
    }
  ]
})

new Vue({
  router,
  render: h => h(App),
}).$mount('#app')
